def traite_input():
    f = open("input", 'r')
    val = f.read()
    f.close()

    values = []

    for i in val :
        values.append(int(i))


    return values

def in_layers(values, n, m):

    t = len(values) /(n*m)

    layers = []

    for i in range(t):
        layers.append(values[(n*m*i) : (n*m*(i+1))])

    return layers

def min_layers(layers, i):

    mini = 25*7
    l = 0 

    for k in range(len(layers)):
        temp = 0 
        for j in layers[k] :
            if j == 0 :
                temp += 1
        
        if temp <    mini : 
            mini = temp 
            l = k 

    return layers[l]
            
def answer(layer):
    nbr_1 = 0 
    nbr_2 = 0 

    for i in layer :
        if i == 1 : 
            nbr_1 += 1 
        if i == 2 :
            nbr_2 += 1 

    return nbr_2 * nbr_1

def make_image_1D(layers):

    image = [] 

    for i in range(len(layers[1])):
        k = 0 
        while layers[k][i] == 2 :
            k += 1 
        image.append(layers[k][i])

    return image 

def make_image(image_1D):
    image = []

    for k in range(6):
        image.append(image_1D[25*k:25*(k+1)])

    return image
    





values = traite_input()

layers = in_layers(values, 25,6)

image_1D = make_image_1D(layers)

image = make_image(image_1D)

for k in image :
    print(k)
    print("\r")
