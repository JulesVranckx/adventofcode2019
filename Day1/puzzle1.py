def fuel_required(mass):
	if mass <= 0 :
		return 0
	temp = mass // 3
	if (temp-2) <= 0 :
		return 0
	return (temp-2) + fuel_required(temp-2) 

sum =0 

f = open("puzzle1.txt")

while(1): 
	mass = f.readline()
	if mass == '' :
		break
	sum += fuel_required(int(mass))
f.close()
print(sum)

print(fuel_required(14))
print(fuel_required(1969))
print(fuel_required(100756))
