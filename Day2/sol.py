
def prog(a,b):	
	f = open("input")
	values = f.readline()
	f.close()


	values = values.split(',')
	values[-1] = '0'

	instr_list = []

	for i in range(len(values)):
		values[i] = int(values[i])
	values[1] = a
	values[2] = b
	
	for i in range(0,len(values),4):
		if values[i] != 99 :
			instr_list.append(values[i:i+4])
		else :
			break 
	
	
	def decode(instr):
		
		if instr[0] == 1 :
			values[instr[3]] = values[instr[1]] + values[instr[2]]
		if instr[0] == 2 :
			values[instr[3]] = values[instr[1]] * values[instr[2]]

	for instr in instr_list :
		decode(instr)

	return values[0]

for i in range(100):
	for j in range(100):
		if prog(i,j) == 19690720 :
			print(i,j)

 


