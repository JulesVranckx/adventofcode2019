f = open("input")
path1 = f.readline()
path2 = f.readline()
f.close()

path1 = path1.split(',')
path2 = path2.split(',')

path1[-1] = path1[-1].split('\n')[0]
path2[-1] = path2[-1].split('\n')[0]

pos = [[(0,0)], [(0,0)]]

def move(j,command):
	
	if command[0] == 'R' :
		n = int(command[1:])
		x = pos[j][-1][0]
		y = pos[j][-1][1]
		for i in range(1,n+1) :
			pos[j].append((x+i,y))

	if command[0] == 'L' :
		n = int(command[1:])
		x = pos[j][-1][0]
		y = pos[j][-1][1]
		for i in range(1,n+1) :
        		pos[j].append((x-i,y))

	if command[0] == 'U' :
		n = int(command[1:])
		x = pos[j][-1][0]
		y = pos[j][-1][1]
		for i in range(1,n+1) :
		        pos[j].append((x,y+i))

	if command[0] == 'D' :
		n = int(command[1:])
		x = pos[j][-1][0]
		y = pos[j][-1][1]
		for i in range(1,n+1) :
		        pos[j].append((x,y-i))

for command in path1 :
	move(0,command)

for command in path2 :
	move(1,command)

for i in range(len(pos[0])):
	for j in range(len(pos[1])):
		if pos[0][i] == pos[1][j]:
			print(i+j)
