def double_in(n):
	s = str(n)
	if s[0] == s[1] and s[1] != s[2] :
		return True
	for p in range(1,len(s)-2):
		if s[p] == s[p+1] and s[p] != s[p+2] and s[p] != s[p-1]:
			return True
	if s[4] == s[5] and s[4] != s[3] :
		return True
	return False


def increase(n):
	s = str(n)
	for p in range(len(s)-1):
		if s[p] > s[p+1] :
			return False
	return True

cmpt = 0 

for i in range(264793, 803936):
	if double_in(i) and increase(i) :
		cmpt += 1 

print(cmpt)
