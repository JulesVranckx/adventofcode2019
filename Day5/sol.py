def traite_input() :
	f = open("input")
	values = f.readline()
	f.close()

	values = values.split(',')
	values[-1] = '0'

	return values

def extend(opcode, m):
	n = len(opcode)
	deb = ''
	for i in range(m-n):
		deb = '0' + deb
	return deb+opcode

def decode(instr, values,length):

		print(instr)

		if (length == 2):

			
			opcode = instr[0]

			if opcode == '3' : 
				s = input("Veuillez entrer une valeur numerique : ")
				values[int(instr[1])] = s 
				return 
			
			elif opcode == '4' :
				s = values[int(instr[1])]
				print("Valeur retournee : " + s)
				return 
			
			elif opcode == '104':
				s = instr[1]
				print("Valeur retournee : " + s)

		elif (length == 3):

			opcode = extend(instr[0], 4)

			#Definition des parametres 
			if opcode[0] == '1' :
				param2 = int(instr[2])
			else :
				param2 = int(values[int(instr[2])])
			
			if opcode[1] == '1' :
				param1 = int(instr[1])
			else :
				param1 = int(values[int(instr[1])])

			if opcode[-1] == '5' and param1 != 0 :
				return param2
			elif opcode[-1] == '6' and param1 == 0 :
				return param2 
			else :
				return -1 		

		elif (length == 4):

			opcode = extend(instr[0], 5)

			#Definition des parametres 
			if opcode[1] == '1' :
				param2 = int(instr[2])
			else :
				param2 = int(values[int(instr[2])])
			
			if opcode[2] == '1' :
				param1 = int(instr[1])
			else :
				param1 = int(values[int(instr[1])])

			#execution
			if opcode[-1] == '1' :
				values[int(instr[3])] = str(param1 + param2)
			
			elif opcode[-1] == '2' :
				values[int(instr[3])] = str(param1 * param2)
			
			elif opcode[-1] == '7' :
				if param1 <= param2 :
					values[int(instr[3])] = '1' 
				else :
					values[int(instr[3])] = '0' 
			
			elif opcode[-1] == '8' :
				if param1 == param2 :
					values[int(instr[3])] = '1' 
				else :
					values[int(instr[3])] = '0' 
					



def execute(values) :
	i = 0 

	while(1):

		print(i, values[i])

		opcode = values[i]

		if opcode[-1] in ['3', '4']:
			decode(values[i:i+2], values,2)
			i += 2 
		elif opcode[-1] in ['1', '2','7','8']:
			decode(values[i:i+4], values,4)
			i += 4
		elif opcode[-1] in ['5', '6']:
			ret = decode(values[i:i+3], values,3)
			if ret == -1 :
				i += 3 
			else :
				i = ret
		elif opcode == '99' :
			return 
		else :
			print(i, values[i])
			print("Oops")
			return

values = traite_input()

execute(values)
